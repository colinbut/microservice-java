FROM openjdk:8-alpine
RUN mkdir /apps
WORKDIR /apps
ADD target/crypto*.jar /apps/app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
